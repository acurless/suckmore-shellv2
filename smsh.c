#define _GNU_SOURCE
#include <errno.h>
#include <getopt.h>
#include <seccomp.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/fcntl.h>
#include <sys/wait.h>
#include <unistd.h>

#define CMD_LEN 64
#define DEFAULT_PROMPT ">"
#define MAX_ARGS 16
#define ARG_DELIM " "
#define RD_BUF 4096
#define MAX_ENVS 8

static const char help[] = "%s - Suckmore Shell v2.0\n"
                           "The latest in linux technologies\n"
                           "Options:\n"
                           "\t-b <file>         Use the contents of <file> as "
                           "a motd style banner.\n";
static char* tmp_env[] = {
    "LD_PRELOAD=/usr/lib/smsh-override.so",
    "LANG=en_US.UTF-8",
    "TERM=xterm",
    "SHLVL=1",
    "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/"
    "usr/bin:/sbin:/bin",
    "TERMCAP=\"vt102|$TERM|dec vt102:\"'\\"
    ":do=^J:co#80:li#24:cl=50\\E[;H\\E[2J:\\"
    ":le=^H:bs:cm=5\\E[%i%d;%dH:nd=2\\E[C:up=2\\E[A:\\"
    ":ce=3\\E[K:cd=50\\E[J:so=2\\E[7m:se=2\\E[m:us=2\\E[4m:ue=2\\E["
    "m:\\"
    ":md=2\\E[1m:mr=2\\E[7m:mb=2\\E[5m:me=2\\E[m:is=\\E[1;24r\\E["
    "24;1H:\\"
    ":rs=\\E>\\E[?3l\\E[?4l\\E[?5l\\E[?7h\\E[?8h:ks=\\E[?1h\\E=:ke="
    "\\E[?1l\\E>:\\"
    ":ku=\\EOA:kd=\\EOB:kr=\\EOC:kl=\\EOD:kb=^H:\\"
    ":ho=\\E[H:k1=\\EOP:k2=\\EOQ:k3=\\EOR:k4=\\EOS:pt:sr=5\\EM:vt#"
    "3:\\"
    ":sc=\\E7:rc=\\E8:cs=\\E[%i%d;%dr:vs=\\E[?7l:ve=\\E[?7h:\\"
    ":mi:al=\\E[L:dc=\\E[P:dl=\\E[M:ei=\\E[4l:im=\\E[4h:",
    NULL};

static scmp_filter_ctx ctx;

void install_filter()
{
    ctx = seccomp_init(SCMP_ACT_ALLOW);

    /* seccomp_rule_add(ctx, SCMP_ACT_TRAP, SCMP_SYS(ioctl), 0); */
    seccomp_rule_add(ctx, SCMP_ACT_TRAP, SCMP_SYS(fcntl), 1,
                     SCMP_A0(SCMP_CMP_GT, 200));

    seccomp_load(ctx);
}

void execute(char* argv[], int reset_env)
{
    int pid;
    if ((pid = fork()) < 0)
    {
        fprintf(stderr, "Error forking: %d\n", errno);
    }
    else if (pid == 0)
    {
        char** env;
        if (reset_env)
        {

            env = tmp_env;
        }
        else
        {
            env = environ;
        }

        install_filter();
        int status = execvpe(argv[0], argv, env);
        if (status == -1)
        {
            fprintf(stderr, "Failed to execute command, error: %d\n", errno);
        }
        exit(1);
    }
    else
    {
        wait(0);
    }
}

int handle_command(char* command, char* prompt)
{
    char* args[MAX_ARGS];
    char* arg = strtok(command, ARG_DELIM);

    int idx = 0;
    while (arg != NULL)
    {
        args[idx] = arg;
        arg = strtok(NULL, ARG_DELIM);
        idx++;
    }
    args[idx] = NULL;

    if (idx <= 0)
    {
        return 1;
    }

    int ret = 1;
    if (strcmp(args[0], "exit") == 0)
    {
        printf("exiting\n");
        exit(0);
    }
    else if (strcmp(args[0], "cd") == 0)
    {
        int status = chdir(args[1]);
        if (status != 0)
        {
            fprintf(stderr, "Unable to change directory, error %d\n", errno);
        }
    }
    else if (strcmp(args[0], "setprompt") == 0)
    {
        memset(prompt, 0, CMD_LEN);
        memcpy(prompt, args[1], strnlen(args[1], CMD_LEN));
        printf("New prompt: %s\n", prompt);
    }
    else if (strcmp(args[0], "gcc") == 0)
    {
        execute(args, 0);
    }
    else
    {
        execute(args, 1);
    }
    return ret;
}

static void handler(int signum)
{
    switch (signum)
    {
    case SIGQUIT:
    case SIGINT:
        break;
    }
}

void print_banner(const char* path)
{
    int banner = open(path, O_RDONLY);
    char buf[RD_BUF] = {0};

    size_t bytes;
    while ((bytes = read(banner, buf, RD_BUF)) > 0)
    {
        int status = write(STDOUT_FILENO, buf, bytes);
        (void)status;
    }

    close(banner);
}

void run_shell()
{
    char prompt[CMD_LEN] = DEFAULT_PROMPT;
    char command[CMD_LEN] = {0};
    char buffer[CMD_LEN] = {0};

    int run_flag = 1;
    while (run_flag)
    {
        printf("%s ", prompt);

        char* cmd = fgets(buffer, CMD_LEN, stdin);
        if (cmd != NULL)
        {
            size_t cpy_len = strnlen(buffer, CMD_LEN) - 1;
            strncpy(command, buffer, cpy_len);
            run_flag = handle_command(command, prompt);
            memset(command, 0, CMD_LEN);
        }
        else
        {
            run_flag = 0;
        }
        memset(buffer, 0, CMD_LEN);
    }
}

int main(int argc, char* argv[])
{
    struct sigaction sa;
    sa.sa_handler = handler;
    sigaction(SIGINT, &sa, NULL);

    int c = 0;
    while ((c = getopt(argc, argv, "b:h")) != -1)
    {
        switch (c)
        {
        case 'b':
        {
            print_banner(optarg);
            break;
        }
        case 'h':
        {
            printf(help, argv[0]);
            return 0;
        }
        default:
        {
            break;
        }
        }
    }

    seccomp_release(ctx);
    run_shell();
    return 0;
}
