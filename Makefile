CC = gcc
CFLAGS = -Wall -Wextra -Werror -Wpedantic -O2 -fPIC -g
LINK = -lrt -lseccomp -ldl

OBJECT_DIR = obj
INCLUDE = -I./filter
SRC = $(wildcard *.c)

OBJ = $(patsubst %.c,$(OBJECT_DIR)/%.o,$(SRC))

ARTIFACT = smsh
FILTER_LIB = smsh-override.so


.PHONY: all src clean build run
all: src

build:
	docker build -t smsh:latest .

run:
	docker run -it smsh:latest

$(OBJECT_DIR)/%.o: smsh.c
	$(CC) -c $(INCLUDE) -o $@ $< $(CFLAGS)

$(ARTIFACT): smsh.o
	$(CC) -o $@ $^ $(LINK)

$(FILTER_LIB): smsh-override.c
	$(CC) -o $@ $^ -ldl -shared

init:
	@mkdir -p $(OBJECT_DIR)

src: init $(FILTER_LIB) $(ARTIFACT)

clean:
	rm -rf $(OBJ) $(ARTIFACT) $(FILTER_LIB)
