#define _GNU_SOURCE
#include <dlfcn.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <sys/stat.h>

typedef __mode_t mode_t;

typedef int (*sys_open_t)(const char* pathname, int flags, mode_t mode);
typedef int (*sys_open_nm_t)(const char* pathname, int flags);
typedef int (*openat_nm_t)(int fd, const char* pathname, int flags);
typedef int (*openat_t)(int fd, const char* pathname, int flags, mode_t mode);

static const char* filter[] = {"flag", "bash", "sh", NULL};

int pass_filter(const char* path)
{
    size_t i = 0;
    while (filter[i] != NULL)
    {
        if (strstr(path, filter[i]) != NULL)
        {
            return 0;
        }
        i++;
    }
    return 1;
}

int open(const char* pathname, int flags)
{
    if (pass_filter(pathname))
    {
        sys_open_nm_t orig = (sys_open_nm_t)dlsym(RTLD_NEXT, "open");
        return orig(pathname, flags);
    }

    return EPERM;
}

/* int open(const char* pathname, int flags, mode_t mode) */
/* { */
/* if (pass_filter(pathname) == 1) */
/* { */
/* sys_open_t orig = (sys_open_t)dlsym(RTLD_NEXT, "open"); */
/* return orig(pathname, flags, mode); */
/* } */
/* return EPERM; */
/* } */

int openat(int fd, const char* pathname, int flags)
{
    if (pass_filter(pathname) == 1)
    {
        openat_nm_t orig = (openat_nm_t)dlsym(RTLD_NEXT, "openat");
        return orig(fd, pathname, flags);
    }
    return EPERM;
}

/* int openat(int fd, const char* pathname, int flags, mode_t mode) */
/* { */
/* if (pass_filter(pathname)) */
/* { */
/* openat_t orig = (openat_t)dlsym(RTLD_NEXT, "openat"); */
/* return orig(fd, pathname, flags, mode); */
/* } */
/* return EPERM; */
/* } */
