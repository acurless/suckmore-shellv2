FROM archlinux:latest
MAINTAINER Adrian Curless (awcurless@wpi.edu)

RUN pacman -Syu --noconfirm
RUN pacman -S --noconfirm gcc make vi

RUN useradd -ms /bin/bash ctf

RUN mkdir /root/smsh
ADD . /root/smsh

WORKDIR /root/smsh
RUN make
RUN install -o 0 -g 0 -m 0555 smsh /usr/bin/smsh
RUN install -o ctf -g ctf -m 444 flag /home/ctf/flag
RUN install -o 0 -g 0 -m 0555 smsh-override.so /usr/lib/smsh-override.so
RUN rm -rf /root/smsh

USER ctf

WORKDIR /home/ctf
ENTRYPOINT /usr/bin/smsh
